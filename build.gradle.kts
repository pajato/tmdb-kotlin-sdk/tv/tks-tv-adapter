plugins {
    alias(libs.plugins.kmp.lib)
}

group = "com.pajato.tks"
version = "0.10.7"
description = "The TMDb Kotlin Sdk (tks) tv feature, interface adapters layer, KMP common target project"

kotlin.sourceSets {
    val commonMain by getting {
        dependencies {
            implementation(libs.kotlinx.coroutines.core)
            implementation(libs.kotlinx.serialization.json)

            implementation(libs.tks.common.core)
            implementation(libs.tks.common.adapter)
            implementation(libs.tks.episode.core)
            implementation(libs.tks.episode.adapter)
            implementation(libs.tks.season.core)
            implementation(libs.tks.season.adapter)
            implementation(libs.tks.tv.core)
        }
    }

    val commonTest by getting {
        dependencies {
            implementation(libs.kotlin.test)
            implementation(libs.pajato.test)
        }
    }
}
