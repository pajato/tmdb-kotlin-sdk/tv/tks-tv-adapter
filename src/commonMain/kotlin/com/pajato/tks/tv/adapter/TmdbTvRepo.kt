package com.pajato.tks.tv.adapter

import com.pajato.tks.common.adapter.Strategy
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.TvKey
import com.pajato.tks.tv.core.Tv
import com.pajato.tks.tv.core.TvRepo

/**
 * TmdbTvRepo is an implementation of the TvRepo interface used to fetch TV-related data from the TMDb API.
 *
 * This object integrates with the TmdbFetcher to perform network requests and handle data retrieval.
 */
public object TmdbTvRepo : TvRepo {

    /**
     * Fetches a television show from the TMDB API based on the provided TvKey.
     *
     * @param key The key representing the television show to be fetched.
     * @return The fetched television show.
     */
    public override suspend fun getTv(key: TvKey): Tv {
        val path = "tv/${key.id}"
        val queryParams: List<String> = listOf()
        val serializer: Strategy<Tv> = Tv.serializer()
        return TmdbFetcher.fetch(path, queryParams, key, serializer, Tv())
    }
}
