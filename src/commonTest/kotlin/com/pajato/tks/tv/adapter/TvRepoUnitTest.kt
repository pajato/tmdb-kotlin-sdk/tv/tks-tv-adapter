package com.pajato.tks.tv.adapter

import com.pajato.test.ReportingTestProfiler
import com.pajato.tks.common.adapter.TmdbApiService.TMDB_BASE_API3_URL
import com.pajato.tks.common.adapter.TmdbFetcher
import com.pajato.tks.common.core.InjectError
import com.pajato.tks.common.core.TvKey
import com.pajato.tks.common.core.jsonFormat
import com.pajato.tks.tv.core.Tv
import kotlinx.coroutines.runBlocking
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import kotlin.test.fail

class TvRepoUnitTest : ReportingTestProfiler() {
    private val apiKey = "8G9B0WX31T2U7P0Q"
    private val key = TvKey(424)
    private val url = "$TMDB_BASE_API3_URL/tv/424?api_key=$apiKey"
    private val resourceName = "torchwood_video.json"
    private val urlConverterMap: Map<String, String> = mapOf(url to resourceName)

    private lateinit var errorMessage: String
    private lateinit var errorExc: String
    private lateinit var errorExcMessage: String
    private var isFetched: Boolean = false

    @BeforeTest fun setUp() {
        TmdbFetcher.inject(::fetch, ::handleError)
        TmdbFetcher.inject(apiKey)
        errorMessage = ""
        errorExc = ""
        errorExcMessage = ""
        isFetched = false
    }

    @Test fun `When accessing a non-cached tv show, verify the show is fetched`() {
        val expectedTv: Tv = jsonFormat.decodeFromString(getJson(resourceName))
        runBlocking {
            assertEquals(expectedTv, TmdbTvRepo.getTv(key))
            assertTrue(isFetched)
        }
    }

    @Test fun `When simulating a tv fetch without injection, verify an inject error is thrown`() {
        val expected = "No api key has been injected!: No repo implementation has been injected!"
        TmdbFetcher.reset()
        runBlocking {
            assertFailsWith<InjectError> { TmdbTvRepo.getTv(key) }.also { assertEquals(expected, it.message) }
        }
    }

    private fun fetch(key: String): String {
        val name = urlConverterMap[key] ?: fail("Key $key is not mapped to a test resource name!")
        isFetched = true
        return getJson(name).ifEmpty { throw IllegalStateException("No JSON available!") }
    }

    private fun getJson(name: String): String = javaClass.classLoader.getResource(name)?.readText() ?: ""

    private fun handleError(message: String, exc: Exception?) {
        errorMessage = message
        errorExc = if (exc == null) "null" else exc.javaClass.name
        errorExcMessage = if (exc == null) "null" else exc.message ?: "null"
    }
}
