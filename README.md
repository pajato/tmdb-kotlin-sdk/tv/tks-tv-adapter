# tks-tv-adapter

## Description

The Movie Database (TMDb) tv feature, interface adapters layer, Kotlin MultiPlatform (KMP) project providing
Android, iOS and Desktop targets.

This project implements TMDb tv interfaces in the interface adapters Clean Architecture layer. It exists to
specify these artifacts which are used by other TMDb interfaces and by outer layers to provide data acquired from the
TMDb database.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on the TMDB API, see
[The Movie Database API](https://developers.themoviedb.org/3/getting-started/introduction).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix | Test Case Name                                                                 |
|-----------------|--------------------------------------------------------------------------------|
| TvRepo          | When accessing a non-cached tv, verify tv is fetched                           |
|                 | When simulating a tv fetch without injection, verify an inject error is thrown |

### Overview

### Notes

The single responsibility for this project is to provide the TMDb tv artifact definitions used by this
and outer architectural layers. The adapter layer implements these adapter tv interfaces.
